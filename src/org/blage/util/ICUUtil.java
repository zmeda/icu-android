package org.blage.util;

import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ICUUtil {
	/**
	 * Recursively sets a {@link Typeface} to all {@link TextView}s in a
	 * {@link ViewGroup}.
	 */
	public static final void setAppFont(ViewGroup container, Typeface font) {
		if (container == null || font == null) {
			return;
		}

		final int childCount = container.getChildCount();

		// Loop through all of the children.
		for (int i = 0; i < childCount; ++i) {
			final View child = container.getChildAt(i);
			if (child instanceof TextView) {
				// Set the font if it is a TextView.
				((TextView) child).setTypeface(font);
			} else if (child instanceof ViewGroup) {
				// Recursively attempt another ViewGroup.
				setAppFont((ViewGroup) child, font);
			}
		}
	}
}
