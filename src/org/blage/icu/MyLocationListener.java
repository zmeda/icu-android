package org.blage.icu;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

public class MyLocationListener implements LocationListener {
	private ClientService mClientMap;

	public MyLocationListener(Context context) {
		mClientMap = (ClientService) context;
	}

	@Override
	public void onLocationChanged(Location location) {
		mClientMap.setMyLocation(location);
	}

	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

}
