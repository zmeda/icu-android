package org.blage.icu;

import org.blage.icu.chat.ChatActivity;
import org.blage.icu.data.Friends;
import org.blage.icu.data.user.Friend;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.Log;
import android.view.MotionEvent;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;

public class MyFriendsOverlay extends Overlay {
	private static final String TAG = "MyFriendsOverlay";

	private Context mContext;

	private Bitmap mFriendsPin;
	private Paint mPaint;

	private GeoPoint mFriendGeoPoint;

	private Friends mFriends;

	public MyFriendsOverlay(Context context) {
		super();

		mContext = context;

		mPaint = new Paint();

		ClientMapActivity clientMapActivity = (ClientMapActivity) mContext;

		mFriendsPin = clientMapActivity.getFriendsPin();
		mFriends = clientMapActivity.getMyFriends();
	}

	@Override
	public boolean onTap(GeoPoint geoPoint, MapView mapView) {
		Projection projection = mapView.getProjection();
		Point tapPoint = new Point();
		projection.toPixels(geoPoint, tapPoint);
		
		int pinWidth = mFriendsPin.getWidth();
		int pinHeight = mFriendsPin.getHeight();
		
		double distance = -1d;
		Friend tappedFriend = null;

		if (mFriends != null) {
			for (Friend friend : mFriends.getFriends()) {
				mFriendGeoPoint = new GeoPoint(((Double) (friend.getLocation().getLatitude() * 1E6)).intValue(),
						((Double) (friend.getLocation().getLongitude() * 1E6)).intValue());

				Point friendsPoint = new Point();

				projection.toPixels(mFriendGeoPoint, friendsPoint);
				
				int absDeltaX = Math.abs(friendsPoint.x - tapPoint.x);
				if (absDeltaX < (pinWidth / 2)) {
					int deltaY = friendsPoint.y - tapPoint.y;
					if (deltaY < pinHeight) {
						deltaY -= pinHeight / 2;
						double newDistance = Math.sqrt(Math.pow(absDeltaX, 2d) + Math.pow(deltaY, 2d));
						if (tappedFriend == null || newDistance < distance) {
							tappedFriend = friend;
							distance = newDistance;
						}
					}
				}
			}
		}
		
		if (tappedFriend != null) {
			Intent intent = new Intent(mContext, ChatActivity.class);
			intent.putExtra(IcuConstants.CHAT_FROM, ((ClientMapActivity)mContext).getMyData());
			intent.putExtra(IcuConstants.CHAT_TO, tappedFriend);
			mContext.startActivity(intent);
		}
		
		return true;
	}

	/**
	 * Test whether an information balloon should be displayed or a prior
	 * balloon hidden.
	 */
	private Friend getHitMapFriend(MapView mapView, GeoPoint tapPoint) {

		// Track which MapLocation was hit...if any
		Friend hitMapFriend = null;

		/*
		 * RectF hitTestRecr = new RectF(); Point screenCoords = new Point();
		 * for(FriendBean friend : mFriends.getFriends()) { // Translate the
		 * MapLocation's lat/long coordinates to screen coordinates
		 * mapView.getProjection().toPixels(friend.getLocation(), screenCoords);
		 * 
		 * // Create a 'hit' testing Rectangle w/size and coordinates of our
		 * icon // Set the 'hit' testing Rectangle with the size and coordinates
		 * of our on screen icon
		 * hitTestRecr.set(-bubbleIcon.getWidth()/2,-bubbleIcon
		 * .getHeight(),bubbleIcon.getWidth()/2,0);
		 * hitTestRecr.offset(screenCoords.x,screenCoords.y);
		 * 
		 * // Finally test for a match between our 'hit' Rectangle and the
		 * location clicked by the user
		 * mapView.getProjection().toPixels(tapPoint, screenCoords);
		 * //hitMapLocation = testLocation; if
		 * (hitTestRecr.contains(screenCoords.x,screenCoords.y)) { hitMapFriend
		 * = testLocation; first = true; isNameAddHold = true; break; } }
		 * testX=(int)screenCoords.x; testY=(int)screenCoords.y; // Lastly clear
		 * the newMouseSelection as it has now been processed tapPoint = null;
		 * if(hitMapFriend==null && selectedMapLocation!=null) return
		 * selectedMapLocation;
		 */

		return hitMapFriend;

	}

	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		Projection projection = mapView.getProjection();

		Friends myFriends = ((ClientMapActivity) mContext).getMyFriends();

		if (!shadow && myFriends != null) {
			for (Friend friend : myFriends.getFriends()) {
				mFriendGeoPoint = new GeoPoint(((Double) (friend.getLocation().getLatitude() * 1E6)).intValue(),
						((Double) (friend.getLocation().getLongitude() * 1E6)).intValue());

				Point friendsPoint = new Point();

				projection.toPixels(mFriendGeoPoint, friendsPoint);

				mPaint.setAlpha((int) ((1 - friend.getAge()) * 255));
				canvas.drawBitmap(mFriendsPin, friendsPoint.x - (mFriendsPin.getWidth() / 2), friendsPoint.y - mFriendsPin.getHeight(), mPaint);
			}
		}
		super.draw(canvas, mapView, shadow);
	}

	@Override
	public boolean onTouchEvent(MotionEvent e, MapView mapView) {
		if (e.getAction() == MotionEvent.ACTION_POINTER_UP) {
			Projection projection = mapView.getProjection();

		}
		return super.onTouchEvent(e, mapView);
	}

}
