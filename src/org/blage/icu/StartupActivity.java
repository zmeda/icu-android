package org.blage.icu;

import java.util.Arrays;
import java.util.UUID;

import org.blage.icu.activity.IcuActivity;
import org.blage.icu.data.IcuUuid;
import org.blage.icu.data.user.Me;
import org.blage.icu.retrieveuuid.RetrieveUUIDActivity;

import android.app.Activity;
import android.app.backup.BackupManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.urbanairship.push.PushManager;
import com.urbanairship.push.PushPreferences;

public class StartupActivity extends IcuActivity {
	private static final String TAG = "StartupActivity";

	// This is for debugging purposes only! Enable this to force generating new
	// UUID.
	private static final boolean FETCH_NEW_UUID = false;

	// Intent request codes used in this activity
	private static final int INTENT_REQUEST_CODE_CLIENT_MAP = 1;
	private static final int INTENT_REQUEST_CODE_RETRIEVE_UUID = 2;
	private static final int INTENT_REQUEST_CODE_NEW_OR_EXISTING_USER = 3;

	// Intent extras name
	public static final String INTENT_EXTRA_MY_DATA_NAME = "myData";

	// Shared Preferences for UUID
	private static final String PREFERENCE_FILENAME = "MyPrefsFile";
	private static final String PREFERENCE_MY_UUID_KEY = "MyUUID";
	private static final String PREFERENCE_MY_SHORT_UUID_KEY = "MyShortUUID";
	private static final String PREFERENCE_MY_APID_KEY = "MyApid";

	private BackupManager mBackupManager;
	private SharedPreferences mSettings;
	private Me mMyData;
	private LocationManager mLocationManager;

	private TextView mStartupProgressText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.startup);

		mMyData = new Me();

		initViewMembers();

		// Acquire a reference to the system Location Manager
		mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		mMyData.setLocation(getLastKnownLocation(mLocationManager));

		Log.d(TAG, "locatiom = " + mMyData.getLocation());

		try {
			mBackupManager = new BackupManager(this);
		} catch (NoClassDefFoundError e) {
			// This means that we are below Android version 2.2
			mBackupManager = null;
		}

		mSettings = getSharedPreferences(PREFERENCE_FILENAME, 0);
		if (mSettings.contains(PREFERENCE_MY_UUID_KEY) && mSettings.contains(PREFERENCE_MY_SHORT_UUID_KEY)) {
			Log.d(TAG, "UUID = " + mSettings.getString(PREFERENCE_MY_UUID_KEY, null));
			Log.d(TAG, "ShortUUID = " + mSettings.getString(PREFERENCE_MY_SHORT_UUID_KEY, null));
		}
		if (mSettings.contains(PREFERENCE_MY_UUID_KEY) && mSettings.contains(PREFERENCE_MY_SHORT_UUID_KEY) && !FETCH_NEW_UUID) {
			IcuUuid myUUID = new IcuUuid();
			myUUID.setUUID(UUID.fromString(mSettings.getString(PREFERENCE_MY_UUID_KEY, null)));
			myUUID.setShortUUID(mSettings.getString(PREFERENCE_MY_SHORT_UUID_KEY, null));
			if (mSettings.contains(PREFERENCE_MY_APID_KEY)) {
				mMyData.setApid(mSettings.getString(PREFERENCE_MY_APID_KEY, null));
			} else {
				PushPreferences pushPreferences = PushManager.shared().getPreferences();
				mMyData.setApid(pushPreferences.getPushId());
				SharedPreferences.Editor editor = mSettings.edit();
				editor.putString(PREFERENCE_MY_APID_KEY, mMyData.getApid());
				editor.commit();
				doBackup();
			}
			myUUID.setShortUUID(mSettings.getString(PREFERENCE_MY_SHORT_UUID_KEY, null));
			mMyData.setUuid(myUUID);
			new FetchUserHistoryAsyncTask().execute(myUUID);
		} else {
			Intent newOrExistingUserActivityIntent = new Intent(this, NewOrExistingUserActivity.class);
			startActivityForResult(newOrExistingUserActivityIntent, INTENT_REQUEST_CODE_NEW_OR_EXISTING_USER);
		}
	}

	private void initViewMembers() {
		mStartupProgressText = (TextView) findViewById(R.id.startup_progress_text);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case INTENT_REQUEST_CODE_CLIENT_MAP:
				finish();
				break;
			case INTENT_REQUEST_CODE_RETRIEVE_UUID:
				if (resultCode == Activity.RESULT_OK) {
					if (data != null) {
						mMyData.setUuid((IcuUuid) data.getParcelableExtra(RetrieveUUIDActivity.INTENT_EXTRA_MY_UUID_NAME));
						SharedPreferences.Editor editor = mSettings.edit();
						editor.putString(PREFERENCE_MY_SHORT_UUID_KEY, mMyData.getUuid().getShortUUID());
						editor.putString(PREFERENCE_MY_UUID_KEY, mMyData.getUuid().getUUID().toString());
						editor.commit();
						doBackup();
					}
				} else {
					finish();
				}
				break;
			case INTENT_REQUEST_CODE_NEW_OR_EXISTING_USER:
				if (resultCode == Activity.RESULT_OK) {
					if (data != null) {
						Log.d(TAG, String.valueOf(data.getBooleanExtra(NewOrExistingUserActivity.INTENT_EXTRA_NEW_USER, false)));
						if (data.getBooleanExtra(NewOrExistingUserActivity.INTENT_EXTRA_NEW_USER, false)) {
							new FetchUuidAsyncTask().execute((Void) null);
						} else {
							Intent retrieveUUIDActivityIntent = new Intent(this, RetrieveUUIDActivity.class);
							startActivityForResult(retrieveUUIDActivityIntent, INTENT_REQUEST_CODE_RETRIEVE_UUID);
						}
					}
				} else {
					finish();
				}
				break;
			default:
				super.onActivityResult(requestCode, resultCode, data);
				break;
		}
	}

	private class FetchUuidAsyncTask extends AsyncTask<Void, Void, IcuUuid> {
		@Override
		protected void onPreExecute() {
			mStartupProgressText.setText(R.string.fetching_uuid);
		}

		@Override
		protected IcuUuid doInBackground(Void... params) {
			return getIcuApplication().getCommunicationProxy().getUUIDAndShortUUID();
		}

		@Override
		protected void onPostExecute(IcuUuid result) {
			mMyData.setUuid(result);
			PushPreferences pushPreferences = PushManager.shared().getPreferences();
			mMyData.setApid(pushPreferences.getPushId());
			SharedPreferences.Editor editor = mSettings.edit();
			editor.putString(PREFERENCE_MY_UUID_KEY, result.getUUID().toString());
			editor.putString(PREFERENCE_MY_SHORT_UUID_KEY, result.getShortUUID());
			editor.putString(PREFERENCE_MY_APID_KEY, mMyData.getApid());
			editor.commit();
			doBackup();
			mStartupProgressText.setText(null);
			mMyData.setHistory(new String[] {});
			new FetchFavoritesAsyncTask().execute((Void) null);
		}
	}

	private void doBackup() {
		if (mBackupManager != null) {
			mBackupManager.dataChanged();
		}
	}

	private Location getLastKnownLocation(final LocationManager locationManager) {
		Location lastKnownLocation = null;
		Location lastKnownLocationGPS = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

		Location lastKnownLocationNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

		Location lastKnownLocationPassive = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

		if (lastKnownLocationGPS != null) {
			if (lastKnownLocationNetwork != null) {
				if (lastKnownLocationPassive != null) {
					if (lastKnownLocationGPS.getTime() > lastKnownLocationNetwork.getTime()
							&& lastKnownLocationGPS.getTime() > lastKnownLocationPassive.getTime()) {
						lastKnownLocation = lastKnownLocationGPS;
					} else if (lastKnownLocationNetwork.getTime() > lastKnownLocationPassive.getTime()) {
						lastKnownLocation = lastKnownLocationNetwork;
					} else {
						lastKnownLocation = lastKnownLocationPassive;
					}
				} else {
					if (lastKnownLocationGPS.getTime() > lastKnownLocationNetwork.getTime()) {
						lastKnownLocation = lastKnownLocationGPS;
					} else {
						lastKnownLocation = lastKnownLocationNetwork;
					}
				}
			} else {
				if (lastKnownLocationPassive != null) {
					if (lastKnownLocationGPS.getTime() > lastKnownLocationPassive.getTime()) {
						lastKnownLocation = lastKnownLocationGPS;
					} else {
						lastKnownLocation = lastKnownLocationPassive;
					}
				} else {
					lastKnownLocation = lastKnownLocationGPS;
				}
			}
		} else {
			if (lastKnownLocationNetwork != null) {
				if (lastKnownLocationPassive != null) {
					if (lastKnownLocationNetwork.getTime() > lastKnownLocationPassive.getTime()) {
						lastKnownLocation = lastKnownLocationNetwork;
					} else {
						lastKnownLocation = lastKnownLocationPassive;
					}
				} else {
					lastKnownLocation = lastKnownLocationNetwork;
				}
			} else {
				if (lastKnownLocationPassive != null) {
					lastKnownLocation = lastKnownLocationPassive;
				}
			}
		}

		return lastKnownLocation;
	}

	private class FetchUserHistoryAsyncTask extends AsyncTask<IcuUuid, Void, String[]> {
		@Override
		protected void onPreExecute() {
			mStartupProgressText.setText(R.string.fetching_history);
		}

		@Override
		protected String[] doInBackground(IcuUuid... params) {
			return getIcuApplication().getCommunicationProxy().getUserHistory(params[0]);
		}

		@Override
		protected void onPostExecute(String[] result) {
			if (result == null) {
				Log.d(TAG, "history result = null");
			} else {
				Log.d(TAG, "history result = " + Arrays.toString(result) + ", length = " + result.length);
			}
			mMyData.setHistory(result);
			mStartupProgressText.setText(null);
			new FetchFavoritesAsyncTask().execute((Void) null);
		}
	}

	private class FetchFavoritesAsyncTask extends AsyncTask<Void, Void, String[]> {
		@Override
		protected void onPreExecute() {
			mStartupProgressText.setText(R.string.fetching_favorites);
		}

		@Override
		protected String[] doInBackground(Void... params) {
			return getIcuApplication().getCommunicationProxy().getFavorites();
		}

		@Override
		protected void onPostExecute(String[] result) {
			Log.d(TAG, "favorites = " + Arrays.toString(result));
			mMyData.setFavorites(result);
			mStartupProgressText.setText(R.string.loading_map);
			Intent intentClientMapActivity = new Intent(StartupActivity.this, ClientMapActivity.class);
			intentClientMapActivity.putExtra(INTENT_EXTRA_MY_DATA_NAME, mMyData);
			startActivityForResult(intentClientMapActivity, INTENT_REQUEST_CODE_CLIENT_MAP);
		}
	}
}
