package org.blage.icu;

import java.io.ByteArrayInputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.security.auth.x500.X500Principal;

import org.blage.icu.data.Communicable;
import org.blage.icu.data.CommunicableFactory;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.util.Log;

import com.urbanairship.UAirship;
import com.urbanairship.push.PushManager;
import com.urbanairship.push.PushPreferences;

public class IcuApplication extends Application {
	private static final X500Principal DEBUG_CERTIFICATE = new X500Principal("CN=Android Debug,O=Android,C=US");

	private boolean debug;

	private Typeface typeface;

	private BroadcastReceiver broadcastReceiver;

	private Communicable communicationProxy;

	@Override
	public void onCreate() {
		super.onCreate();

		typeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto/Roboto-Regular.ttf");

		broadcastReceiver = new UAIntentReceiver();
		registerBroadcastReceiver();

		UAirship.takeOff(this); // always needs to be called when starting an
								// application

		// even though we call takeoff push notifications are disabled by
		// default
		// you have to explicitly turn them on like so
		PushManager.enablePush();

		// let the push manager know about our intent receiver
		PushManager.shared().setIntentReceiver(UAIntentReceiver.class);

		// set some preferences for sounds and vibration
		PushPreferences pushPreferences = PushManager.shared().getPreferences();
		pushPreferences.setSoundEnabled(true);
		pushPreferences.setVibrateEnabled(true);

		// apid is a unique identifier for a particular device
		String pushId = pushPreferences.getPushId();
		Log.d("ICUApplication", "My Application onCreate - App APID: " + pushId);

		try {
			Signature raw = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES).signatures[0];
			CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
			X509Certificate certificate = (X509Certificate) certificateFactory.generateCertificate(new ByteArrayInputStream(raw.toByteArray()));
			debug = certificate.getSubjectX500Principal().equals(DEBUG_CERTIFICATE);
		} catch (NameNotFoundException e) {
			debug = false;
		} catch (CertificateException e) {
			debug = false;
		}
		communicationProxy = CommunicableFactory.createCommunicable(!debug);
	}

	public boolean isDebug() {
		return debug;
	}

	public Typeface getApplicationTypeface() {
		return typeface;
	}

	public void registerBroadcastReceiver() {
		registerReceiver(broadcastReceiver, new IntentFilter());
	}

	public void unregisterBroadcastReceiver() {
		unregisterReceiver(broadcastReceiver);
	}

	public Communicable getCommunicationProxy() {
		return communicationProxy;
	}
}
