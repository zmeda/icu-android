package org.blage.icu;

import org.blage.icu.activity.IcuActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;

public class NewOrExistingUserActivity extends IcuActivity {
	private static final String TAG = "NewOrExistingUserActivity";

	// boolean value weather is it new user (true) or existing one (false)
	public static final String INTENT_EXTRA_NEW_USER = "newUser";

	private View mNewButton;
	private View mExistingButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.new_or_existing_user);

		setResult(RESULT_CANCELED);

		mNewButton = findViewById(R.id.new_or_existing_user_new);
		mNewButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent newIntent = new Intent();
				newIntent.putExtra(INTENT_EXTRA_NEW_USER, true);
				setResult(RESULT_OK, newIntent);
				finish();
			}
		});

		mExistingButton = findViewById(R.id.new_or_existing_user_existing);
		mExistingButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent existingIntent = new Intent();
				existingIntent.putExtra(INTENT_EXTRA_NEW_USER, false);
				setResult(RESULT_OK, existingIntent);
				finish();
			}
		});
	}
}
