package org.blage.icu;

import android.content.Context;
import android.widget.SlidingDrawer.OnDrawerCloseListener;
import android.widget.SlidingDrawer.OnDrawerOpenListener;

public class SearchSlidingDrawerListeners implements OnDrawerCloseListener, OnDrawerOpenListener {
	private Context mContext;

	public SearchSlidingDrawerListeners(Context context) {
		mContext = context;
	}

	@Override
	public void onDrawerOpened() {
		((ClientMapActivity) mContext).setSearchLayoutShown(true);
	}

	@Override
	public void onDrawerClosed() {
		((ClientMapActivity) mContext).setSearchLayoutShown(false);
	}
}
