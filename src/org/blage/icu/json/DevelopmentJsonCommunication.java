package org.blage.icu.json;

public class DevelopmentJsonCommunication extends AbstractJsonCommunication {
	private final String serverUrl = "http://icu-backend.herokuapp.com";

	@Override
	protected String getServerUrl() {
		return serverUrl;
	}

}
