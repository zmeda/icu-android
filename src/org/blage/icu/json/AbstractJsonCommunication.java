package org.blage.icu.json;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.blage.icu.data.Communicable;
import org.blage.icu.data.Friends;
import org.blage.icu.data.Hint;
import org.blage.icu.data.IcuUuid;
import org.blage.icu.data.chat.Messageable;
import org.blage.icu.data.user.Friend;
import org.blage.icu.data.user.Me;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONStringer;
import org.json.JSONTokener;

import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

public abstract class AbstractJsonCommunication implements Communicable {
	private static final String TAG = "AbstractJsonData";

	private static final String URL_UUID_POSTFIX = "/uuid";
	private static final String URL_SHORT_UUID_POSTFIX = "/short_uuid/";
	private static final String URL_JOIN_POSTFIX = "/1/join.json";
	private static final String URL_JOIN_PRIVATE_POSTFIX = "/join";
	private static final String URL_USER_HISTORY_POSTFIX = "/user_history/";
	private static final String URL_FAVORITES_POSTFIX = "/start.json";
	private static final String URL_RETRIEVE_UUID_POSTFIX = "/retrieve_uuid";
	private static final String URL_HINTS_POSTFIX = "/hints";
	private static final String URL_MESSAGE_POSTFIX = "/message";

	// Constants for sending JSON to server
	private static final String JSON_FRIENDS_UUID = "uuid";
	private static final String JSON_FRIENDS_KEYWORD = "keyword";
	private static final String JSON_FRIENDS_LATITUDE = "lat";
	private static final String JSON_FRIENDS_LONGITUDE = "lng";
	private static final String JSON_FRIENDS_PRIVATE = "private";
	private static final String JSON_FRIENDS_DISTANCE = "dist";
	private static final String JSON_FRIENDS_AGE = "age";
	private static final String JSON_FRIENDS_RADIUS = "radius";
	private static final String JSON_FRIENDS_USER_LIST = "users";
	private static final String JSON_FRIENDS_USERNAME = "un";
	private static final String JSON_RETRIEVE_UUID_SHORT_UUID = "short_uuid";
	private static final String JSON_RETRIEVE_UUID_KEYWORD1 = "kw1";
	private static final String JSON_RETRIEVE_UUID_KEYWORD2 = "kw2";
	private static final String JSON_RETRIEVE_UUID_KEYWORD3 = "kw3";
	private static final String JSON_HINT_UUID = "uuid";
	private static final String JSON_HINT_LATITUDE = "latitude";
	private static final String JSON_HINT_LONGITUDE = "longitude";
	private static final String JSON_MESSAGE_TO_UUID = "to_uuid";
	private static final String JSON_MESSAGE_FROM_UUID = "from_uuid";
	private static final String JSON_MESSAGE_TO_APID = "to_apid";
	private static final String JSON_MESSAGE_FROM_APID = "from_apid";
	private static final String JSON_MESSAGE_CONTENT = "content";

	private static final String EMPTY_JSON = "{}";

	public AbstractJsonCommunication() {
		super();
	}

	protected abstract String getServerUrl();

	@Override
	public Friends getFriends(Me myData) {
		Friends friends = new Friends();
		List<Friend> friendsList = new ArrayList<Friend>();
		friends.setFriends(friendsList);

		try {
			// In database latitude: 46,0837093, longitude: 14,4712134
			JSONStringer json = new JSONStringer();
			json.object().key(JSON_FRIENDS_UUID).value(myData.getUuid().getUUID().toString()).key(JSON_FRIENDS_KEYWORD).value(myData.getKeyword())
					.key(JSON_FRIENDS_LATITUDE).value(myData.getLocation().getLatitude()).key(JSON_FRIENDS_LONGITUDE)
					.value(myData.getLocation().getLongitude()).key(JSON_FRIENDS_PRIVATE).value(myData.isPrivateJoin()).endObject();

			HttpResponse httpResponse = doPost(getUrlJoin(), json);
			long responseContentLength = httpResponse.getEntity().getContentLength();

			if (responseContentLength != 0) {
				BufferedReader reader = getHttpResponseContentBufferedReader(httpResponse);

				String stringResponse = reader.readLine();
				if (!isJSONEmpty(stringResponse)) {
					JSONObject jsonResponse = new JSONObject(stringResponse);
					JSONObject jsonMyUser = jsonResponse.getJSONObject("my_user");
					IcuUuid uuidBean = new IcuUuid();
					uuidBean.setUUID(UUID.fromString(jsonMyUser.getString("uuid")));
					uuidBean.setShortUUID(jsonMyUser.getString("un"));
					// TODO IF if exists and backup it
					myData.setUuid(uuidBean);
					// SharedPreferences.Editor editor = mSettings.edit();
					// editor.putString(PREFERENCE_MY_UUID_KEY,
					// result.getUUID().toString());
					// editor.putString(PREFERENCE_MY_SHORT_UUID_KEY,
					// result.getShortUUID());
					// editor.putString(PREFERENCE_MY_APID_KEY,
					// mMyData.getApid());
					// editor.commit();
					// doBackup();
					myData.setHistory(new String[] {});

					friends.setRadius(Float.valueOf(jsonResponse.getString(JSON_FRIENDS_RADIUS)));
					JSONTokener friendTokener = new JSONTokener(jsonResponse.getString(JSON_FRIENDS_USER_LIST));
					JSONArray jsonFriendArray = new JSONArray(friendTokener);

					for (int jsonFrinedArrayIndex = 0; jsonFrinedArrayIndex < jsonFriendArray.length(); jsonFrinedArrayIndex++) {
						JSONObject jsonFriend = jsonFriendArray.getJSONObject(jsonFrinedArrayIndex);

						Friend bean = new Friend();
						bean.setDistance(Float.valueOf(jsonFriend.getString(JSON_FRIENDS_DISTANCE)));
						Location location = new Location(LocationManager.GPS_PROVIDER);
						location.setLatitude(Double.valueOf(jsonFriend.getString(JSON_FRIENDS_LATITUDE)));
						location.setLongitude(Double.valueOf(jsonFriend.getString(JSON_FRIENDS_LONGITUDE)));
						bean.setLocation(location);
						bean.setAge(Double.valueOf(jsonFriend.getString(JSON_FRIENDS_AGE)));
						// TODO Proper property for username
						bean.setKeyword(jsonFriend.getString(JSON_FRIENDS_USERNAME));

						friendsList.add(bean);
					}
				}
			}
		} catch (JSONException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return friends;
	}

	@Override
	public IcuUuid getUUIDAndShortUUID() {
		BufferedReader reader = null;
		IcuUuid myUUID = new IcuUuid();
		try {
			HttpResponse responseUUID = doGet(getUrlUuid());
			reader = getHttpResponseContentBufferedReader(responseUUID);
			String line = reader.readLine();
			myUUID.setUUID(UUID.fromString(line));
			HttpResponse responseShortUUID = doGet(getUrlShortUuid() + line);
			reader = getHttpResponseContentBufferedReader(responseShortUUID);
			line = reader.readLine();
			myUUID.setShortUUID(line);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			close(reader);
		}
		return myUUID;
	}

	@Override
	public String[] getUserHistory(IcuUuid myUUID) {
		BufferedReader reader = null;
		String[] myHistory = {};
		try {
			HttpResponse httpResponseUserHistory = doGet(getUrlUserHistory() + myUUID.getUUID().toString());
			if (httpResponseUserHistory.getEntity().getContentLength() != 0) {
				reader = getHttpResponseContentBufferedReader(httpResponseUserHistory);
				String stringResponse = reader.readLine();
				if (!isJSONEmpty(stringResponse)) {
					JSONTokener historyTokener = new JSONTokener(stringResponse);
					JSONArray jsonHistoryArray = new JSONArray(historyTokener);
					myHistory = new String[jsonHistoryArray.length()];
					for (int jsonHistoryArrayIndex = 0; jsonHistoryArrayIndex < jsonHistoryArray.length(); jsonHistoryArrayIndex++) {
						myHistory[jsonHistoryArrayIndex] = jsonHistoryArray.getString(jsonHistoryArrayIndex);
					}
				}
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			close(reader);
		}
		return myHistory;
	}

	@Override
	public String[] getFavorites() {
		String[] favorites = { "coffee", "lunch", "talk", "drink", "Java" };
		/*
		 * BufferedReader reader = null; try { HttpResponse
		 * httpResponseFavorites = doGet(getUrlFavorites()); if
		 * (httpResponseFavorites.getEntity().getContentLength() != 0) { reader
		 * = getHttpResponseContentBufferedReader(httpResponseFavorites); String
		 * stringResponse = reader.readLine(); if (!isJSONEmpty(stringResponse))
		 * { JSONObject jsonResponse = new JSONObject(stringResponse); JSONArray
		 * topJoins = jsonResponse.getJSONArray("top_joins");
		 * 
		 * favorites = new String[topJoins.length()]; for (int
		 * jsonFavoritesArrayIndex = 0; jsonFavoritesArrayIndex <
		 * favorites.length; jsonFavoritesArrayIndex++) {
		 * favorites[jsonFavoritesArrayIndex] =
		 * topJoins.getString(jsonFavoritesArrayIndex); } } } } catch
		 * (ClientProtocolException e) { e.printStackTrace(); } catch
		 * (IOException e) { e.printStackTrace(); } catch (JSONException e) {
		 * e.printStackTrace(); } finally { close(reader); }
		 */
		return favorites;
	}

	@Override
	public IcuUuid retrieveUUID(String shortUUID, String keyword1, String keyword2, String keyword3) {
		BufferedReader reader = null;
		JSONStringer json = new JSONStringer();
		IcuUuid myUUID = null;
		try {
			json.object().key(JSON_RETRIEVE_UUID_SHORT_UUID).value(shortUUID).key(JSON_RETRIEVE_UUID_KEYWORD1).value(keyword1).key(JSON_RETRIEVE_UUID_KEYWORD2)
					.value(keyword2).key(JSON_RETRIEVE_UUID_KEYWORD3).value(keyword3).endObject();
			HttpResponse retrieveUUIDHttpResponse = doPost(getUrlRetrieveUuid(), json);
			if (retrieveUUIDHttpResponse.getEntity().getContentLength() != 0) {
				reader = getHttpResponseContentBufferedReader(retrieveUUIDHttpResponse);
				String stringResponse = reader.readLine();
				if (!isJSONEmpty(stringResponse) && stringResponse.length() == 36) {
					myUUID = new IcuUuid();
					myUUID.setShortUUID(shortUUID);
					myUUID.setUUID(UUID.fromString(stringResponse));
				}
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			close(reader);
		}
		return myUUID;
	}

	@Override
	public Hint[] getHints(Me myData) {
		Hint hint = new Hint();
		hint.setKeyword("test123");
		Hint[] hints = { hint };
		/*
		 * BufferedReader reader = null; JSONStringer json = new JSONStringer();
		 * try {
		 * json.object().key(JSON_HINT_UUID).value(myData.getUuid().getUUID
		 * ().toString
		 * ()).key(JSON_HINT_LATITUDE).value(myData.getLocation().getLatitude())
		 * .key(JSON_HINT_LONGITUDE).value(myData.getLocation().getLongitude()).
		 * endObject(); HttpResponse httpResponseHints = doPost(geturlHints(),
		 * json); if (httpResponseHints.getEntity().getContentLength() != 0) {
		 * reader = getHttpResponseContentBufferedReader(httpResponseHints);
		 * String stringResponse = reader.readLine(); if
		 * (!isJSONEmpty(stringResponse)) { JSONTokener hintsTokener = new
		 * JSONTokener(stringResponse); JSONArray jsonHintsArray = new
		 * JSONArray(hintsTokener); hints = new Hint[jsonHintsArray.length()];
		 * for (int jsonHintsArrayIndex = 0; jsonHintsArrayIndex <
		 * jsonHintsArray.length(); jsonHintsArrayIndex++) { Hint hint = new
		 * Hint();
		 * hint.setKeyword(jsonHintsArray.getString(jsonHintsArrayIndex));
		 * hints[jsonHintsArrayIndex] = hint; Log.d(TAG, "hint = " + hint); } }
		 * } } catch (ClientProtocolException e) { e.printStackTrace(); } catch
		 * (IOException e) { e.printStackTrace(); } catch (JSONException e) {
		 * e.printStackTrace(); } finally { close(reader); }
		 */
		return hints;
	}

	@Override
	public void sendMessage(Messageable from, Messageable to, String message) {
		BufferedReader reader = null;
		JSONStringer json = new JSONStringer();
		try {
			json.object().key(JSON_MESSAGE_FROM_APID).value(from.getApid()).key(JSON_MESSAGE_FROM_UUID).value(from.getUuid().getUUID().toString())
					.key(JSON_MESSAGE_TO_APID).value(to.getApid()).key(JSON_MESSAGE_TO_UUID).value(to.getUuid().getUUID().toString()).key(JSON_MESSAGE_CONTENT)
					.value(message).endObject();
			HttpResponse httpResponseMessage = doPost(getUrlMessage(), json);
			Log.d(TAG, "json = " + json.toString());
			Log.d(TAG, httpResponseMessage.getStatusLine().toString());
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} finally {
			close(reader);
		}

	}

	private HttpResponse doPost(String url, JSONStringer json) throws ClientProtocolException, IOException {
		HttpPost request = new HttpPost(url);
		StringEntity entity;
		entity = new StringEntity(json.toString());

		entity.setContentType("application/json;charset=UTF-8");
		entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json;charset=UTF-8"));
		request.setEntity(entity);

		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpResponse response = httpClient.execute(request);
		return response;
	}

	private HttpResponse doGet(String url) throws ClientProtocolException, IOException {
		HttpGet request = new HttpGet(url);

		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpResponse response = httpClient.execute(request);
		return response;
	}

	private void close(BufferedReader bufferedReader) {
		try {
			if (bufferedReader != null) {
				bufferedReader.close();
			}
		} catch (IOException e) {
		}
	}

	private BufferedReader getHttpResponseContentBufferedReader(HttpResponse httpResponse) throws UnsupportedEncodingException, IllegalStateException,
			IOException {
		return new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
	}

	private boolean isJSONEmpty(String json) {
		return (json == null || json.length() == 0 || EMPTY_JSON.equals(json));
	}

	private String getUrlUuid() {
		return getServerUrl() + URL_UUID_POSTFIX;
	}

	private String getUrlShortUuid() {
		return getServerUrl() + URL_SHORT_UUID_POSTFIX;
	}

	private String getUrlJoin() {
		return getServerUrl() + URL_JOIN_POSTFIX;
	}

	private String getUrlJoinPrivate() {
		return getServerUrl() + URL_JOIN_PRIVATE_POSTFIX;
	}

	private String getUrlUserHistory() {
		return getServerUrl() + URL_USER_HISTORY_POSTFIX;
	}

	private String getUrlFavorites() {
		return getServerUrl() + URL_FAVORITES_POSTFIX;
	}

	private String getUrlRetrieveUuid() {
		return getServerUrl() + URL_RETRIEVE_UUID_POSTFIX;
	}

	private String getUrlHints() {
		return getServerUrl() + URL_HINTS_POSTFIX;
	}

	private String getUrlMessage() {
		return getServerUrl() + URL_MESSAGE_POSTFIX;
	}
}
