package org.blage.icu;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.urbanairship.UAirship;
import com.urbanairship.push.PushManager;

public class UAIntentReceiver extends BroadcastReceiver {
	private static final String TAG = "UAIntentReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "onReceive()");
		String action = intent.getAction();
		Log.d(TAG, "action = " + action);
		if (action.equals(PushManager.ACTION_NOTIFICATION_OPENED)) {
			Log.d(TAG, "Action Notification Opened");
			// user opened the notification so we launch the application

			// This intent is what will be used to launch the activity in our
			// application
			Intent launchIntent = new Intent(Intent.ACTION_MAIN);

			// Main.class can be substituted any activity in your android
			// project that you wish
			// to be launched when the user selects the notification from the
			// Notifications drop down
			/* TODO Create proper Activity for pushes */
			launchIntent.setClass(UAirship.shared().getApplicationContext(), ClientMapActivity.class);
			launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			// copy the intent data from the incoming intent to the intent
			// that we are going to launch
			//copyIntentData(intent, launchIntent);
			launchIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

			UAirship.shared().getApplicationContext().startActivity(launchIntent);

		} else if (action.equals(PushManager.ACTION_PUSH_RECEIVED)) {
			Log.d(TAG, "Action Push Received");
			// TODO Ziga Do something useful with push notification
			// Probably some pin blinking or for start just draw a bubble or sth. above user
			
			// Get content of message
			Object message = intent.getExtras().get("message");
			
			// By who message came from
			Object from = intent.getExtras().get("from");
			
			// And we make some toast ;)
			Toast.makeText(context, (String)message, Toast.LENGTH_LONG).show();
			Log.d(TAG, "extra message = " + message + ", extra from = " + from);
		}
	}
}
