package org.blage.icu.retrieveuuid;

import org.blage.icu.R;
import org.blage.icu.data.IcuUuid;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Toast;

public class RetrieveButtonOnTouchListener implements OnTouchListener {
	private static final String TAG = "RetrieveButtonOnTouchListener";

	private RetrieveUUIDActivity mRetrieveUUIDActivity;
	private int oldAction = -1;

	public RetrieveButtonOnTouchListener(Context context) {
		super();
		mRetrieveUUIDActivity = (RetrieveUUIDActivity) context;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (oldAction != event.getAction()) {
			oldAction = event.getAction();
			Log.d(TAG, "new action = " + oldAction);
		}
		if (event.getAction() == MotionEvent.ACTION_UP) {
			changeImage(false);
			String shortUUIDCharSequence = mRetrieveUUIDActivity.mShortUUIDTextView.getText().toString();
			if (shortUUIDCharSequence == null || shortUUIDCharSequence.length() < 5) {
				mRetrieveUUIDActivity.mShortUUIDTextView.requestFocus();
				Toast.makeText(mRetrieveUUIDActivity, R.string.retrieve_uuid_no_short_uuid, Toast.LENGTH_LONG).show();
				return true;
			}

			ProgressDialog dialog = ProgressDialog.show(mRetrieveUUIDActivity, "", "Loading. Please wait...", true);
			IcuUuid myUUID = mRetrieveUUIDActivity
					.getIcuApplication()
					.getCommunicationProxy()
					.retrieveUUID(shortUUIDCharSequence, convertTo(mRetrieveUUIDActivity.mKeywordOneTextView.getText()),
							convertTo(mRetrieveUUIDActivity.mKeywordTwoTextView.getText()), convertTo(mRetrieveUUIDActivity.mKeywordThreeTextView.getText()));
			dialog.dismiss();
			Intent dataIntent = new Intent();
			dataIntent.putExtra(RetrieveUUIDActivity.INTENT_EXTRA_MY_UUID_NAME, myUUID);
			mRetrieveUUIDActivity.setResult(Activity.RESULT_OK, dataIntent);
			mRetrieveUUIDActivity.finish();
		} else if (event.getAction() == MotionEvent.ACTION_DOWN) {
			changeImage(true);
			return true;
		} else if (event.getAction() == MotionEvent.ACTION_CANCEL || event.getAction() == MotionEvent.ACTION_OUTSIDE) {
			changeImage(false);
			return true;
		}
		return true;
	}

	private void changeImage(boolean pressed) {
		if (pressed) {
			mRetrieveUUIDActivity.mRetrieveButtonImage.setImageResource(R.drawable.retrieve_bottom_pressed);
		} else {
			mRetrieveUUIDActivity.mRetrieveButtonImage.setImageResource(R.drawable.retrieve_bottom);
		}
	}

	private String convertTo(CharSequence value) {
		return (value != null && value.length() > 0) ? value.toString() : "";
	}
}
