package org.blage.icu.retrieveuuid;

import org.blage.icu.R;
import org.blage.icu.activity.IcuActivity;

import android.app.Activity;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class RetrieveUUIDActivity extends IcuActivity {
	// Log tag
	private static final String TAG = "RetrieveUUIDActivity";

	// Intent extras name
	public static final String INTENT_EXTRA_MY_UUID_NAME = "myUUID";

	// Interactive interface objects (view members)
	TextView mShortUUIDTextView;
	TextView mKeywordOneTextView;
	TextView mKeywordTwoTextView;
	TextView mKeywordThreeTextView;
	private View mRetrieveButton;
	ImageView mRetrieveButtonImage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.retreive_uuid);

		// Set default return activity code
		// In case of user presses back button
		setResult(Activity.RESULT_CANCELED);

		initViewMemebers();
	}

	private void initViewMemebers() {
		mShortUUIDTextView = (TextView) findViewById(R.id.retrieve_uuid_short_uuid);
		InputFilter[] inputFilters = new InputFilter[1];
		inputFilters[0] = new InputFilter.LengthFilter(5);
		mShortUUIDTextView.setFilters(inputFilters);

		mKeywordOneTextView = (TextView) findViewById(R.id.retrieve_uuid_keyword_one);
		mKeywordTwoTextView = (TextView) findViewById(R.id.retrieve_uuid_keyword_two);
		mKeywordThreeTextView = (TextView) findViewById(R.id.retrieve_uuid_keyword_three);

		mRetrieveButton = findViewById(R.id.retrieve_uuid_retrieve);
		mRetrieveButton.setOnTouchListener(new RetrieveButtonOnTouchListener(this));

		mRetrieveButtonImage = (ImageView) findViewById(R.id.retrieve_uuid_retrieve_image);
	}
}
