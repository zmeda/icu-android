package org.blage.icu;

public class IcuConstants {
	public static final boolean SUPPORT_FROYO = android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.FROYO;
	
	public static final String CHAT_FROM = "chatFrom";
	public static final String CHAT_TO = "chatTo";
	
	public static final String GOOGLE_ANALYTICS_UA_NUMBER = "UA-29245544-1";
	public static final int GOOGLE_ANAYTICS_DISPATCH_INTERVAL = 30;
}
