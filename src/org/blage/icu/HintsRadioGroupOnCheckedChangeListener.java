package org.blage.icu;

import android.content.Context;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class HintsRadioGroupOnCheckedChangeListener implements OnCheckedChangeListener {
	private Context mContext;

	public HintsRadioGroupOnCheckedChangeListener(Context context) {
		mContext = context;
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		if (checkedId != View.NO_ID) {
			ClientMapActivity clientMap = (ClientMapActivity) mContext;
			String keyword = ((RadioButton) clientMap.findViewById(checkedId)).getText().toString();
			clientMap.setKeyword(keyword);
			
			RadioGroup favoritesRadioGroup = clientMap.getFavoritesRadioGroup();

			for (int index = 0; index < favoritesRadioGroup.getChildCount(); index++) {
				RadioButton child = (RadioButton) favoritesRadioGroup.getChildAt(index);
				if (child.getText().toString().equalsIgnoreCase(keyword)) {
					favoritesRadioGroup.check(child.getId());
					break;
				}
			}
		}
	}

}
