package org.blage.icu.data;

import org.blage.icu.data.chat.Messageable;
import org.blage.icu.data.user.Me;

public interface Communicable {
	public Friends getFriends(Me myData);

	public IcuUuid getUUIDAndShortUUID();

	public String[] getUserHistory(IcuUuid myUUID);

	public String[] getFavorites();

	public IcuUuid retrieveUUID(String shortUUID, String keyword1, String keyword2, String keyword3);

	public Hint[] getHints(Me myData);
	
	public void sendMessage(Messageable from, Messageable to, String message);
}
