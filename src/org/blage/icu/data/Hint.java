package org.blage.icu.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Hint implements Parcelable {
	private String keyword;
	private int meters;

	public Hint() {
	}

	public Hint(String keyword, int meters) {
		this.keyword = keyword;
		this.meters = meters;
	}

	private Hint(Parcel in) {
		keyword = in.readString();
		meters = in.readInt();
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public int getMeters() {
		return meters;
	}

	public void setMeters(int meters) {
		this.meters = meters;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(keyword);
		out.writeInt(meters);
	}

	public static final Parcelable.Creator<Hint> CREATOR = new Parcelable.Creator<Hint>() {
		public Hint createFromParcel(Parcel in) {
			return new Hint(in);
		}

		public Hint[] newArray(int size) {
			return new Hint[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}
}
