package org.blage.icu.data.chat;

import org.blage.icu.data.IcuUuid;

public interface Messageable {
	public String getApid();

	public IcuUuid getUuid();
}
