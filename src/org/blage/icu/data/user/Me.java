package org.blage.icu.data.user;

import java.util.Arrays;

import org.blage.icu.data.Hint;
import org.blage.icu.data.IcuUuid;
import org.blage.util.EqualsUtil;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.android.maps.GeoPoint;

public class Me extends AbstractUser implements Parcelable {
	private static final String TAG = "MeBean";

	private GeoPoint locationGeoPoint;
	private String[] history;
	private String[] favorites;
	private Hint[] hints;
	private boolean privateJoin;

	public Me() {
	}

	private Me(Parcel in) {
		setUuid((IcuUuid) in.readParcelable(getClass().getClassLoader()));
		setKeyword(in.readString());
		setLocation((Location) in.readParcelable(getClass().getClassLoader()));
		setApid(in.readString());
		int historyLength = in.readInt();
		if (historyLength > 0) {
			history = new String[historyLength];
			in.readStringArray(history);
		}
		int favoritesLength = in.readInt();
		Log.d(TAG, "construstor favs length = " + favoritesLength);
		if (favoritesLength > 0) {
			favorites = new String[favoritesLength];
			in.readStringArray(favorites);
			Log.d(TAG, "construstor favs = " + Arrays.toString(favorites));
		}
		int hintsLength = in.readInt();
		Log.d(TAG, "constructor hints length = " + hintsLength);
		if (hintsLength > 0) {
			hints = new Hint[hintsLength];
			Parcelable[] hints = in.readParcelableArray(Hint.class.getClassLoader());
			for (int hintIndex = 0; hintIndex < hints.length; hintIndex++) {
				hints[hintIndex] = (Hint) hints[hintIndex];
			}
		}
		privateJoin = in.readByte() == 0 ? false : true;
	}

	@Override
	public void setLocation(Location location) {
		super.setLocation(location);

		setLocationGeoPoint(location);
	}

	private void setLocationGeoPoint(Location location) {
		if (location != null) {
			locationGeoPoint = new GeoPoint(((Double) (location.getLatitude() * 1E6)).intValue(), ((Double) (location.getLongitude() * 1E6)).intValue());
		}
	}

	public GeoPoint getLocationGeoPoint() {
		return locationGeoPoint;
	}

	public void setHistory(String[] history) {
		this.history = history;
	}

	public String[] getHistory() {
		return history;
	}

	public void setFavorites(String[] favorites) {
		this.favorites = favorites;
	}

	public String[] getFavorites() {
		return favorites;
	}

	public void setHints(Hint[] hints) {
		this.hints = hints;
	}

	public Hint[] getHints() {
		return hints;
	}

	public boolean isPrivateJoin() {
		return privateJoin;
	}

	public void setPrivateJoin(boolean privateJoin) {
		this.privateJoin = privateJoin;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		Log.d(TAG, toString());
		out.writeParcelable(getUuid(), flags);
		out.writeString(getKeyword());
		out.writeParcelable(getLocation(), flags);
		out.writeString(getApid());
		if (history != null && history.length > 0) {
			out.writeInt(history.length);
			out.writeStringArray(history);
		} else {
			out.writeInt(-1);
		}
		if (favorites != null && favorites.length > 0) {
			out.writeInt(favorites.length);
			out.writeStringArray(favorites);
		} else {
			out.writeInt(-1);
		}
		if (hints != null && hints.length > 0) {
			out.writeInt(hints.length);
			out.writeParcelableArray(hints, flags);
		} else {
			out.writeInt(-1);
		}
		out.writeByte(privateJoin ? (byte) 1 : (byte) 0);
	}

	public static final Parcelable.Creator<Me> CREATOR = new Parcelable.Creator<Me>() {
		public Me createFromParcel(Parcel in) {
			return new Me(in);
		}

		public Me[] newArray(int size) {
			return new Me[size];
		}
	};

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append(super.toString());
		sb.append(", locationGeoPoint = ").append(locationGeoPoint == null ? "null" : locationGeoPoint.toString());
		sb.append(", history = ").append(Arrays.toString(history));
		sb.append(", favories = ").append(Arrays.toString(favorites));
		sb.append(", privateJoin = ").append(privateJoin);
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (!(o instanceof Me)) {
			return false;
		}

		final Me other = (Me) o;
		return EqualsUtil.areEqual(this.locationGeoPoint, other.locationGeoPoint) && EqualsUtil.areEqual(this.history, other.history)
				&& EqualsUtil.areEqual(this.favorites, other.favorites) && EqualsUtil.areEqual(this.getUuid(), other.getUuid())
				&& EqualsUtil.areEqual(this.privateJoin, other.privateJoin) && super.equals(other);
	}
}
