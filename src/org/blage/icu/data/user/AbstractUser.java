package org.blage.icu.data.user;

import org.blage.icu.data.IcuUuid;
import org.blage.icu.data.chat.Messageable;
import org.blage.util.EqualsUtil;

import android.location.Location;

public abstract class AbstractUser implements Messageable {
	private IcuUuid uuid;
	private String keyword;
	private Location location;
	private String apid;

	public AbstractUser() {
	}

	@Override
	public String getApid() {
		return apid;
	}

	public void setApid(String apid) {
		this.apid = apid;
	}

	@Override
	public IcuUuid getUuid() {
		return uuid;
	}

	public void setUuid(IcuUuid uuid) {
		this.uuid = uuid;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (!(o instanceof AbstractUser)) {
			return false;
		}

		final AbstractUser other = (AbstractUser) o;
		return EqualsUtil.areEqual(this.keyword, other.keyword) && EqualsUtil.areEqual(this.location, other.location)
				&& EqualsUtil.areEqual(this.uuid, other.uuid) && EqualsUtil.areEqual(this.apid, other.apid);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("UUID = ").append(uuid == null ? "null" : uuid.toString());
		sb.append(", keyword = ").append(keyword);
		sb.append(", location = ").append(location == null ? "null" : location.toString());
		sb.append(", apid = ").append(apid);
		return sb.toString();
	}
}
