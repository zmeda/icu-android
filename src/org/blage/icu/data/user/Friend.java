package org.blage.icu.data.user;

import org.blage.icu.data.IcuUuid;
import org.blage.util.EqualsUtil;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;

public class Friend extends AbstractUser implements Parcelable {
	private float distance;
	private double age;

	public Friend() {
		super();
	}

	private Friend(Parcel in) {
		setUuid((IcuUuid) in.readParcelable(Me.class.getClassLoader()));
		setKeyword(in.readString());
		setLocation((Location) in.readParcelable(Me.class.getClassLoader()));
		setApid(in.readString());
		distance = in.readFloat();
		age = in.readDouble();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeParcelable(getUuid(), flags);
		out.writeString(getKeyword());
		out.writeParcelable(getLocation(), flags);
		out.writeString(getApid());
		out.writeFloat(distance);
		out.writeDouble(age);
	}

	public static final Parcelable.Creator<Friend> CREATOR = new Parcelable.Creator<Friend>() {
		public Friend createFromParcel(Parcel in) {
			return new Friend(in);
		}

		public Friend[] newArray(int size) {
			return new Friend[size];
		}
	};

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public double getAge() {
		return age;
	}

	public void setAge(double age) {
		this.age = age;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (!(o instanceof Friend)) {
			return false;
		}

		final Friend other = (Friend) o;
		return EqualsUtil.areEqual(this.distance, other.distance) && EqualsUtil.areEqual(this.age, other.age) && super.equals(other);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("(");
		sb.append(super.toString());
		sb.append(", distance: ").append(distance);
		sb.append(", age: ").append(age);
		sb.append(")");
		return sb.toString();
	}

}
