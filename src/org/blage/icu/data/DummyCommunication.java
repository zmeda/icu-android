package org.blage.icu.data;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.blage.icu.data.chat.Messageable;
import org.blage.icu.data.user.Friend;
import org.blage.icu.data.user.Me;

import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

public class DummyCommunication implements Communicable {
	private static final String TAG = "DummyCommunication";

	@Override
	public Friends getFriends(Me myData) {
		Log.d(TAG, "getFriends");
		Friends friends = new Friends();
		friends.setRadius(200f);
		List<Friend> friendsList = new ArrayList<Friend>();
		friends.setFriends(friendsList);
		Friend friend1 = new Friend();
		friend1.setDistance(150f);
		Location location = new Location(LocationManager.GPS_PROVIDER);
		location.setLatitude(myData.getLocation().getLatitude() + 0.001);
		location.setLongitude(myData.getLocation().getLongitude());
		friend1.setLocation(location);
		friend1.setAge(0.1);
		friend1.setKeyword(myData.getKeyword());
		friendsList.add(friend1);
		return friends;
	}

	@Override
	public IcuUuid getUUIDAndShortUUID() {
		Log.d(TAG, "getUUIDAndShortUUID");
		IcuUuid uuid = new IcuUuid();
		uuid.setShortUUID("A1B2C");
		uuid.setUUID(UUID.randomUUID());
		return uuid;
	}

	@Override
	public String[] getUserHistory(IcuUuid myUUID) {
		Log.d(TAG, "getUserHistory");
		return new String[] { "history1", "history2", "history3" };
	}

	@Override
	public String[] getFavorites() {
		Log.d(TAG, "getFavorites");
		return new String[] { "fav1", "fav2", "fav3" };
	}

	@Override
	public IcuUuid retrieveUUID(String shortUUID, String keyword1, String keyword2, String keyword3) {
		Log.d(TAG, "retrieveUUID");
		return null;
	}

	@Override
	public Hint[] getHints(Me myData) {
		Log.d(TAG, "getHints");
		return new Hint[] { new Hint("hint1", 10), new Hint("hint2", 20) };
	}

	@Override
	public void sendMessage(Messageable from, Messageable to, String message) {
		Log.d(TAG, "sendMessage");
	}

}
