package org.blage.icu.data;

import java.util.List;

import org.blage.icu.data.user.Friend;

public class Friends {
	private float mRadius;
	private List<Friend> mFriends;

	public void setRadius(float radius) {
		mRadius = radius;
	}

	public float getRadius() {
		return mRadius;
	}

	public void setFriends(List<Friend> friends) {
		mFriends = friends;
	}

	public List<Friend> getFriends() {
		return mFriends;
	}
}
