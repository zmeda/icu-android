package org.blage.icu.data;

import java.util.UUID;

import org.blage.util.EqualsUtil;

import android.os.Parcel;
import android.os.Parcelable;

public class IcuUuid implements Parcelable {
	private UUID mUUID;
	private String mShortUUID;

	public IcuUuid() {
	}

	private IcuUuid(Parcel in) {
		mUUID = UUID.fromString(in.readString());
		mShortUUID = in.readString();
	}

	public UUID getUUID() {
		return mUUID;
	}

	public void setUUID(UUID uuid) {
		mUUID = uuid;
	}

	public String getShortUUID() {
		return mShortUUID;
	}

	public void setShortUUID(String shortUUID) {
		mShortUUID = shortUUID;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(mUUID.toString());
		out.writeString(mShortUUID);
	}

	public static final Parcelable.Creator<IcuUuid> CREATOR = new Parcelable.Creator<IcuUuid>() {
		public IcuUuid createFromParcel(Parcel in) {
			return new IcuUuid(in);
		}

		public IcuUuid[] newArray(int size) {
			return new IcuUuid[size];
		}
	};
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}

		if (!(o instanceof IcuUuid)) {
			return false;
		}

		final IcuUuid other = (IcuUuid) o;
		return EqualsUtil.areEqual(this.mUUID, other.mUUID) && EqualsUtil.areEqual(this.mShortUUID, other.mShortUUID);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("(");
		sb.append("uuid = ").append(mUUID.toString());
		sb.append(", shortUUID = ").append(mShortUUID);
		sb.append(")");
		return sb.toString();
	}
}
