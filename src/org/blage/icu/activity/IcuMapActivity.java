package org.blage.icu.activity;

import org.blage.icu.IcuApplication;
import org.blage.util.ICUUtil;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.maps.MapActivity;

public abstract class IcuMapActivity extends MapActivity {
	private IcuApplication application;
	protected ViewGroup rootViewGroup;

	protected abstract boolean isRouteDisplayed();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		application = (IcuApplication) getApplication();
	}

	@Override
	public void setContentView(int layoutResID) {
		super.setContentView(layoutResID);
		setDefaultFont();
	}

	@Override
	public void setContentView(View view, LayoutParams params) {
		super.setContentView(view, params);
		setDefaultFont();
	}

	@Override
	public void setContentView(View view) {
		super.setContentView(view);
		setDefaultFont();
	}

	protected void setDefaultFont() {
		if (rootViewGroup == null) {
			rootViewGroup = (ViewGroup) findViewById(android.R.id.content).getRootView();
		}
		ICUUtil.setAppFont(rootViewGroup, application.getApplicationTypeface());
	}

	@Override
	protected void onStart() {
		super.onStart();
		EasyTracker.getInstance().activityStart(this);
	}

	@Override
	protected void onStop() {
		super.onStop();
		EasyTracker.getInstance().activityStop(this);
	}
	
	public IcuApplication getIcuApplication() {
		return application;
	}
}
