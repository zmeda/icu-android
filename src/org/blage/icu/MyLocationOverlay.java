package org.blage.icu;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.util.Log;
import android.widget.ImageView;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;

public class MyLocationOverlay extends Overlay {
	private static final String TAG = "MyLocationOverlay";

	private Context mContext;

	private GeoPoint mMyLocationGeoPoint;

	private Bitmap mMyPin;

	private AnimationDrawable animationChange;

	public MyLocationOverlay(Context context) {
		super();

		mContext = context;

		mMyPin = ((ClientMapActivity) mContext).getMyPin();

		ImageView imageView = new ImageView(mContext);
		imageView.setBackgroundResource(R.drawable.unreaded_message_change);
		animationChange = (AnimationDrawable) imageView.getBackground();

		// animationAlpha = (AnimationDrawable)
		// AnimationUtils.loadAnimation(mContext,
		// R.anim.unreaded_message_alpha);
	}

	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		Projection projection = mapView.getProjection();

		mMyLocationGeoPoint = ((ClientMapActivity) mContext).getMyData().getLocationGeoPoint();

		if (!shadow && mMyLocationGeoPoint != null) {
			Point myPoint = new Point();

			projection.toPixels(mMyLocationGeoPoint, myPoint);

			if (animationChange.isRunning()) {
				// canvas.save();
				// canvas.translate(myPoint.x - (mMyPin.getWidth() / 2),
				// myPoint.y - mMyPin.getHeight());
				// animationChange.getCurrent().draw(canvas);
				// canvas.restore();
				canvas.drawBitmap(((BitmapDrawable) animationChange.getCurrent()).getBitmap(), myPoint.x - (mMyPin.getWidth() / 2),
						myPoint.y - mMyPin.getHeight(), null);
			} else {
				canvas.drawBitmap(mMyPin, myPoint.x - (mMyPin.getWidth() / 2), myPoint.y - mMyPin.getHeight(), null);
			}
		}
		// super.draw(canvas, mapView, shadow);
	}

	public boolean onTap(GeoPoint geoPoint, MapView mapView) {
		Projection projection = mapView.getProjection();
		Point tapPoint = new Point();
		projection.toPixels(geoPoint, tapPoint);

		mMyLocationGeoPoint = ((ClientMapActivity) mContext).getMyData().getLocationGeoPoint();

		int pinWidth = mMyPin.getWidth();
		int pinHeight = mMyPin.getHeight();

		boolean touched = false;

		if (mMyLocationGeoPoint != null) {
			Point myPoint = new Point();

			projection.toPixels(mMyLocationGeoPoint, myPoint);

			int absDeltaX = Math.abs(myPoint.x - tapPoint.x);
			if (absDeltaX < (pinWidth / 2)) {
				int deltaY = myPoint.y - tapPoint.y;
				if (deltaY < pinHeight) {
					touched = true;
				}
			}
		}

		if (touched) {
			Log.d(TAG, "We have been touched! Yeah!");
			animationChange.start();
		}

		return true;
	}
}