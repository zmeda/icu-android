package org.blage.icu;

import java.util.ArrayList;
import java.util.List;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

public class ClientService extends Service {
	private static final String TAG = "ClientService";

	private static final int ONGOING_NOTIFICATION = 1;

	// Constants for IPC
	static final int MSG_REGISTER_CLIENT = 1;
	static final int MSG_UNREGISTER_CLIENT = 2;
	static final int MSG_LOCATION_CHANGED = 3;
	static final int MSG_NO_LOCATION_PROVIDER = 4;

	private List<Messenger> mClients = new ArrayList<Messenger>();
	private Location mMyLocation;
	private LocationManager mLocationManager;
	private LocationListener mLocationListener;

	@Override
	public void onCreate() {
		super.onCreate();
		// Acquire a reference to the system Location Manager
		mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		mLocationListener = new MyLocationListener(this);
		mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 10, mLocationListener);
	}

	/**
	 * Handler of incoming messages from clients.
	 */
	class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
				case MSG_REGISTER_CLIENT:
					Log.d(TAG, "Register Client");
					Messenger client = msg.replyTo;
					mClients.add(client);

					if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
						try {
							client.send(Message.obtain(null, MSG_NO_LOCATION_PROVIDER));
						} catch (RemoteException e) {
							e.printStackTrace();
						}
					} else {
						if (mMyLocation != null) {
							try {
								client.send(Message.obtain(null, MSG_LOCATION_CHANGED, mMyLocation));
							} catch (RemoteException e) {
								e.printStackTrace();
							}
						}
					}
					break;
				case MSG_UNREGISTER_CLIENT:
					Log.d(TAG, "Unregister Client");
					mClients.remove(msg.replyTo);
					break;
				default:
					super.handleMessage(msg);
			}
		}
	}

	/**
	 * Target we publish for clients to send messages to IncomingHandler.
	 */
	final Messenger mMessenger = new Messenger(new IncomingHandler());

	@Override
	public void onDestroy() {
		super.onDestroy();
		stopForeground(true);
		// Remove the listener you previously added
		mLocationManager.removeUpdates(mLocationListener);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Notification notification = new Notification(R.drawable.ic_stat_notify_app, getText(R.string.ticker_text), System.currentTimeMillis());
		Intent notificationIntent = new Intent(this, ClientMapActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
		notification.setLatestEventInfo(this, getText(R.string.notification_title), getText(R.string.notification_message), pendingIntent);
		startForeground(ONGOING_NOTIFICATION, notification);

		// If we get killed, after returning from here, restart
		return START_STICKY;
	}

	/**
	 * When binding to the service, we return an interface to our messenger for
	 * sending messages to the service.
	 */
	@Override
	public IBinder onBind(Intent intent) {
		return mMessenger.getBinder();
	}

	public void setMyLocation(Location myLocation) {
		mMyLocation = myLocation;
		for (int clientIndex = 0; clientIndex < mClients.size(); clientIndex++) {
			try {
				mClients.get(clientIndex).send(Message.obtain(null, MSG_LOCATION_CHANGED, mMyLocation));
			} catch (RemoteException e) {
				// The client is dead. Remove it from the list;
				// we are going through the list from back to front
				// so this is safe to do inside the loop.
				mClients.remove(clientIndex);
			}
		}
	}
}
