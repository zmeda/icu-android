package org.blage.icu;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class SearchButtonOnClickListener implements OnClickListener {
	private Context mContext;

	public SearchButtonOnClickListener(Context context) {
		mContext = context;
	}

	@Override
	public void onClick(View view) {
		InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

		ClientMapActivity clientMap = (ClientMapActivity) mContext;
		String keyword = clientMap.getSearchEditText().getText().toString();
		
		if (clientMap.getFavoritesRadioGroup().getCheckedRadioButtonId() != View.NO_ID) {
			clientMap.getFavoritesRadioGroup().clearCheck();
		}
		
		clientMap.setKeyword(keyword);
		clientMap.populateMyFriendsFromSource();
		clientMap.hideSearchLayout();
		
		RadioGroup favoritesRadioGroup = clientMap.getFavoritesRadioGroup();

		for (int index = 0; index < favoritesRadioGroup.getChildCount(); index++) {
			RadioButton child = (RadioButton) favoritesRadioGroup.getChildAt(index);
			if (child.getText().toString().equalsIgnoreCase(keyword)) {
				favoritesRadioGroup.check(child.getId());
				break;
			}
		}
	}
}
