package org.blage.icu;

import android.app.backup.BackupAgentHelper;
import android.app.backup.SharedPreferencesBackupHelper;

public class IcuBackupAgent extends BackupAgentHelper {
	// The name of the SharedPreferences file
	private static final String PREFERENCE_FILENAME = "MyPrefsFile";

	// A key to uniquely identify the set of backup data
	private static final String PREFERENCE_FILENAME_BACKUP_KEY = "MyPrefsFileBackupKey";

	// Allocate a helper and add it to the backup agent
	@Override
	public void onCreate() {
		SharedPreferencesBackupHelper helper = new SharedPreferencesBackupHelper(this, PREFERENCE_FILENAME);
		addHelper(PREFERENCE_FILENAME_BACKUP_KEY, helper);
	}
}
