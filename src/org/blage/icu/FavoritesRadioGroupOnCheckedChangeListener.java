package org.blage.icu;

import android.content.Context;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

public class FavoritesRadioGroupOnCheckedChangeListener implements OnCheckedChangeListener {
	private Context mContext;

	public FavoritesRadioGroupOnCheckedChangeListener(Context context) {
		mContext = context;
	}

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		if (checkedId != View.NO_ID) {
			ClientMapActivity clientMap = (ClientMapActivity) mContext;
			String keyword = ((RadioButton) clientMap.findViewById(checkedId)).getText().toString();
			clientMap.setKeyword(keyword);
			clientMap.populateMyFriendsFromSource();
			clientMap.hideSearchLayout();
		}
	}
}
