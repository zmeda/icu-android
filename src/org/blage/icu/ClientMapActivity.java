package org.blage.icu;

import java.util.List;

import org.blage.icu.activity.IcuMapActivity;
import org.blage.icu.data.Friends;
import org.blage.icu.data.Hint;
import org.blage.icu.data.user.Friend;
import org.blage.icu.data.user.Me;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SlidingDrawer;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class ClientMapActivity extends IcuMapActivity {
	private static final String TAG = "ClientMapActivity";

	private static final int RADIO_BUTTON_PADDING_LEFT = 50;
	private float mDensity;

	private MapView mMapView;
	private MyCircleOverlay mMyCircleOverlay;
	private MyFriendsOverlay mMyFriendsOverlay;
	private MyLocationOverlay mMyLocationOverlay;
	private TextView mProgressText;
	private ProgressBar mProgressIcon;
	private TextView mShortUuidText;
	private Me mMyData;
	private boolean mSearchLayoutShown;
	private SlidingDrawer mSearchLayout;
	private CheckBox mSearchPrivateJoin;
	private AutoCompleteTextView mSearchEditText;
	private View mSearchButton;
	private Friends mMyFriends;
	private Messenger mService;
	private boolean mIsBound;
	private boolean mIsInitialRun;
	private RadioGroup mFavoritesRadioGroup;
	private RadioGroup mHintsRadioGroup;

	private Bitmap mMyPin;
	private Bitmap mFriendsPin;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "activity onCreate()");

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		mDensity = metrics.density;

		mMyData = getIntent().getExtras().getParcelable(StartupActivity.INTENT_EXTRA_MY_DATA_NAME);

		setContentView(R.layout.main_map);

		mMyPin = BitmapFactory.decodeResource(getResources(), R.drawable.my_pin);
		mFriendsPin = BitmapFactory.decodeResource(getResources(), R.drawable.friends_pin);

		mSearchEditText = (AutoCompleteTextView) findViewById(R.id.search_keyword);
		if (mMyData.getHistory() != null && mMyData.getHistory().length > 0) {
			ArrayAdapter<String> historyAdapter = new ArrayAdapter<String>(this, R.layout.list_history_item, mMyData.getHistory());
			mSearchEditText.setAdapter(historyAdapter);
		}
		mSearchEditText.setHintTextColor(R.color.text_gray);

		mSearchButton = findViewById(R.id.search_button);
		mSearchButton.setOnClickListener(new SearchButtonOnClickListener(this));

		mMapView = (MapView) findViewById(R.id.mapview);
		mMapView.setBuiltInZoomControls(false);

		List<Overlay> mapOverlays = mMapView.getOverlays();

		mMyCircleOverlay = new MyCircleOverlay(this);
		mapOverlays.add(mMyCircleOverlay);

		mMyFriendsOverlay = new MyFriendsOverlay(this);
		mapOverlays.add(mMyFriendsOverlay);

		mMyLocationOverlay = new MyLocationOverlay(this);
		mapOverlays.add(mMyLocationOverlay);

		mMapView.invalidate();

		mSearchLayoutShown = false;
		mSearchLayout = (SlidingDrawer) findViewById(R.id.search_layout);
		SearchSlidingDrawerListeners searchSlidingDrawerListeners = new SearchSlidingDrawerListeners(this);
		mSearchLayout.setOnDrawerCloseListener(searchSlidingDrawerListeners);
		mSearchLayout.setOnDrawerOpenListener(searchSlidingDrawerListeners);
		mSearchLayout.setOnDrawerScrollListener(new SlidingDrawer.OnDrawerScrollListener() {

			@Override
			public void onScrollStarted() {
				Log.d(TAG, "onScrollStarted(), isAnimNull = " + (mSearchLayout.getLayoutAnimationListener() == null ? "true" : "false"));
			}

			@Override
			public void onScrollEnded() {
				Log.d(TAG, "onScrollEnded()");
			}
		});

		mProgressText = (TextView) findViewById(R.id.progress_text);
		mProgressIcon = (ProgressBar) findViewById(R.id.progress_icon);

		mSearchPrivateJoin = (CheckBox) findViewById(R.id.search_private);
		mSearchPrivateJoin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				mMyData.setPrivateJoin(isChecked);
			}
		});
		mMyData.setPrivateJoin(mSearchPrivateJoin.isChecked());

		Intent serviceIntent = new Intent(this, ClientService.class);
		startService(serviceIntent);

		mIsInitialRun = true;

		initFavorites();

		mShortUuidText = (TextView) findViewById(R.id.short_uuid_text);
		mShortUuidText.setVisibility(View.VISIBLE);
		mShortUuidText.setText(mMyData.getUuid().getShortUUID());

		Log.d(TAG, "onCreate()");
	}

	private void initFavorites() {
		mFavoritesRadioGroup = (RadioGroup) findViewById(R.id.favorites);
		mFavoritesRadioGroup.setOnCheckedChangeListener(new FavoritesRadioGroupOnCheckedChangeListener(this));
		ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		for (String favorite : mMyData.getFavorites()) {
			RadioButton favoriteRadioButton = new RadioButton(this);
			favoriteRadioButton.setBackgroundResource(R.drawable.radio_favorite_background);
			favoriteRadioButton.setButtonDrawable(R.drawable.radio_favorite_button);
			favoriteRadioButton.setPadding((int) (mDensity * (float) RADIO_BUTTON_PADDING_LEFT), 0, 0, 0);
			favoriteRadioButton.setLayoutParams(layoutParams);
			favoriteRadioButton.setText(favorite);
			favoriteRadioButton.setTextColor(R.color.text_gray);
			mFavoritesRadioGroup.addView(favoriteRadioButton);
		}
		setDefaultFont();
	}

	private void initHints() {
		mHintsRadioGroup = (RadioGroup) findViewById(R.id.hints);
		mHintsRadioGroup.removeAllViews();
		mHintsRadioGroup.setOnCheckedChangeListener(new HintsRadioGroupOnCheckedChangeListener(this));
		ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		for (Hint hint : mMyData.getHints()) {
			RadioButton hintRadioButton = new RadioButton(this);
			hintRadioButton.setBackgroundResource(R.drawable.radio_hint_background);
			hintRadioButton.setButtonDrawable(R.drawable.radio_hint_button);
			hintRadioButton.setPadding((int) (mDensity * (float) RADIO_BUTTON_PADDING_LEFT), 0, 0, 0);
			hintRadioButton.setLayoutParams(layoutParams);
			hintRadioButton.setText(hint.getKeyword());
			hintRadioButton.setTextColor(R.color.text_gray);
			mHintsRadioGroup.addView(hintRadioButton);
		}
		setDefaultFont();
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.d(TAG, "onStart()");
		Intent intentBind = new Intent(this, ClientService.class);
		bindService(intentBind, mConnection, 0);
	}

	@Override
	protected void onStop() {
		super.onStop();
		if (mIsBound) {
			// If we have received the service, and hence registered with
			// it, then now is the time to unregister.
			if (mService != null) {
				try {
					Message msg = Message.obtain(null, ClientService.MSG_UNREGISTER_CLIENT);
					msg.replyTo = mService;
					mService.send(msg);
				} catch (RemoteException e) {
					// There is nothing special we need to do if the service
					// has crashed.
				}
			}

			// Detach our existing connection.
			unbindService(mConnection);
			mIsBound = false;
		}
		Log.d(TAG, "onStop()");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy()");

		Intent serviceIntent = new Intent(this, ClientService.class);
		stopService(serviceIntent);
	}

	@Override
	protected boolean isRouteDisplayed() {
		return false;
	}

	public void populateMyFriendsFromSource() {
		if (mMyData.getUuid() == null) {
			Toast.makeText(this, R.string.wait_for_uuid, Toast.LENGTH_SHORT).show();
			return;
		}
		if (mMyData.getLocation() == null) {
			Toast.makeText(this, R.string.wait_for_location, Toast.LENGTH_SHORT).show();
			return;
		}
		if (mMyData.getKeyword() == null || mMyData.getKeyword().length() == 0) {
			Toast.makeText(this, R.string.fill_keyword, Toast.LENGTH_SHORT).show();
			return;
		}
		new PopulateMyFriendsAsyncTask().execute(mMyData);
	}

	public Friends getMyFriends() {
		return mMyFriends;
	}

	public void hideSearchLayout() {
		mSearchLayout.animateClose();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
			case KeyEvent.KEYCODE_BACK:
				if (mSearchLayoutShown) {
					hideSearchLayout();
					return true;
				}
				return super.onKeyDown(keyCode, event);
			case KeyEvent.KEYCODE_MENU:
				if (!mSearchLayoutShown) {
					return super.onKeyDown(keyCode, event);
				} else {
					return true;
				}
			default:
				return super.onKeyDown(keyCode, event);
		}
	}

	public EditText getSearchEditText() {
		return mSearchEditText;
	}

	public void setKeyword(String keyword) {
		mMyData.setKeyword(keyword);
		mSearchEditText.setText(keyword);
	}

	public Me getMyData() {
		return mMyData;
	}

	public void setMyData(Me myData) {
		mMyData = myData;
	}

	public RadioGroup getFavoritesRadioGroup() {
		return mFavoritesRadioGroup;
	}

	private ServiceConnection mConnection = new ServiceConnection() {
		public void onServiceConnected(ComponentName className, IBinder service) {
			mService = new Messenger(service);
			mIsBound = true;

			// We want to monitor the service for as long as we are
			// connected to it.
			try {
				Message msg = Message.obtain(null, ClientService.MSG_REGISTER_CLIENT);
				msg.replyTo = mMessenger;
				mService.send(msg);
			} catch (RemoteException e) {
				// In this case the service has crashed before we could even
				// do anything with it; we can count on soon being
				// disconnected (and then reconnected if it can be restarted)
				// so there is no need to do anything here.
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			mService = null;
			mIsBound = false;
		}
	};

	private class PopulateMyFriendsAsyncTask extends AsyncTask<Me, Void, Friends> {
		@Override
		protected void onPreExecute() {
			mProgressIcon.setVisibility(View.VISIBLE);
			mProgressText.setVisibility(View.VISIBLE);
			mProgressText.setText(R.string.fetching_friends);
		}

		@Override
		protected Friends doInBackground(Me... params) {
			Me myData = params[0];
			return getIcuApplication().getCommunicationProxy().getFriends(myData);
		}

		@Override
		protected void onPostExecute(Friends result) {
			mMyFriends = result;
			if (result != null && result.getFriends() != null && result.getFriends().size() > 0) {
				int maxLatitude = mMyData.getLocationGeoPoint().getLatitudeE6();
				int minLatitude = maxLatitude;
				int maxLongitude = mMyData.getLocationGeoPoint().getLongitudeE6();
				int minLongitude = maxLongitude;
				for (Friend bean : result.getFriends()) {
					if (maxLatitude < (bean.getLocation().getLatitude() * 1e6)) {
						maxLatitude = (int) (bean.getLocation().getLatitude() * 1e6);
					} else if (minLatitude > bean.getLocation().getLatitude()) {
						minLatitude = (int) (bean.getLocation().getLatitude() * 1e6);
					}
					if (maxLongitude < bean.getLocation().getLongitude()) {
						maxLongitude = (int) (bean.getLocation().getLongitude() * 1e6);
					} else if (minLongitude > bean.getLocation().getLongitude()) {
						minLongitude = (int) (bean.getLocation().getLongitude() * 1e6);
					}
				}
				mMapView.getController().zoomToSpan((maxLatitude - minLatitude) * 2, (maxLongitude - minLongitude) * 2);
				mMapView.invalidate();
			}
			mMapView.getController().animateTo(mMyData.getLocationGeoPoint());
			mMapView.invalidate();
			mProgressText.setText(null);
			mProgressIcon.setVisibility(View.GONE);
			mProgressText.setVisibility(View.GONE);

			if (!mMyFriends.getFriends().isEmpty()) {
				float[] results = new float[1];
				Location.distanceBetween(mMyData.getLocation().getLatitude(), mMyData.getLocation().getLongitude(), mMyFriends.getFriends().get(0)
						.getLocation().getLatitude(), mMyFriends.getFriends().get(0).getLocation().getLongitude(), results);
			}
			new FetchHintsAsyncTask().execute(mMyData);
		}
	}

	private class FetchHintsAsyncTask extends AsyncTask<Me, Void, Hint[]> {
		@Override
		protected void onPreExecute() {
			mProgressIcon.setVisibility(View.VISIBLE);
			mProgressText.setVisibility(View.VISIBLE);
			mProgressText.setText(R.string.fetching_hints);
		}

		@Override
		protected Hint[] doInBackground(Me... params) {
			return getIcuApplication().getCommunicationProxy().getHints(params[0]);
		}

		@Override
		protected void onPostExecute(Hint[] result) {
			mMyData.setHints(result);
			mProgressText.setText(null);
			mProgressIcon.setVisibility(View.GONE);
			mProgressText.setVisibility(View.GONE);
			initHints();
		}
	}

	// Useful stuff
	// https://github.com/marcust/HHPT/blob/master/src/org/thiesen/hhpt/ui/activity/main/MainActivity.java
	private void launchGPSOptions() {
		final ComponentName toLaunch = new ComponentName("com.android.settings", "com.android.settings.SecuritySettings");
		final Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		intent.addCategory(Intent.CATEGORY_LAUNCHER);
		intent.setComponent(toLaunch);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

	private void buildAlertMessageNoGps() {
		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("Yout GPS seems to be disabled, do you want to enable it?").setCancelable(false)
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(final DialogInterface dialog, final int id) {
						launchGPSOptions();
					}
				}).setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(final DialogInterface dialog, final int id) {
						dialog.cancel();
					}
				});
		final AlertDialog alert = builder.create();
		alert.show();
	}

	public Bitmap getMyPin() {
		return mMyPin;
	}

	public Bitmap getFriendsPin() {
		return mFriendsPin;
	}

	public void setSearchLayoutShown(boolean searchLayoutShown) {
		mSearchLayoutShown = searchLayoutShown;
	}

	final Messenger mMessenger = new Messenger(new IncomingHandler());

	/**
	 * Handler of incoming messages from service.
	 */
	class IncomingHandler extends Handler {
		@Override
		public void handleMessage(Message msg) {
			Log.d(TAG, "handleMessage");
			switch (msg.what) {
				case ClientService.MSG_LOCATION_CHANGED:
					mMyData.setLocation((Location) msg.obj);

					if (mIsInitialRun) {
						mMapView.getController().animateTo(mMyData.getLocationGeoPoint());
						mIsInitialRun = false;
					}

					mMapView.invalidate();
					break;
				case ClientService.MSG_NO_LOCATION_PROVIDER:
					buildAlertMessageNoGps();
					break;
				default:
					super.handleMessage(msg);
			}
		}
	}
}
