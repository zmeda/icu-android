package org.blage.icu.chat;

import org.blage.icu.IcuConstants;
import org.blage.icu.R;
import org.blage.icu.activity.IcuActivity;
import org.blage.icu.data.chat.Messageable;
import org.blage.icu.data.user.Friend;
import org.blage.icu.data.user.Me;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.urbanairship.push.PushManager;

public class ChatActivity extends IcuActivity {
	private ListView mChatList;
	private Button mChatSendButton;
	private EditText mChatInputText;
	private ArrayAdapter<String> mConverstionArrayAdapter;

	private Me mFromMessageable;
	private Friend mToMessageable;

	private ChatBroadcastReceiver mReceiver;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.chat);

		mFromMessageable = getIntent().getExtras().getParcelable(IcuConstants.CHAT_FROM);
		mToMessageable = getIntent().getExtras().getParcelable(IcuConstants.CHAT_TO);

		mChatList = (ListView) findViewById(R.id.chat_list);

		mChatSendButton = (Button) findViewById(R.id.chat_send);
		mChatSendButton.setOnClickListener(new ChatSendOnClickListener(this));

		mChatInputText = (EditText) findViewById(R.id.chat_input);

		mConverstionArrayAdapter = new ArrayAdapter<String>(this, R.layout.chat_item, R.id.chat_text);

		mChatList.setAdapter(mConverstionArrayAdapter);

		mConverstionArrayAdapter.add("Test123");
		mConverstionArrayAdapter.add("Test456");

		mReceiver = new ChatBroadcastReceiver();
	}

	@Override
	protected void onStart() {
		super.onStart();
		getIcuApplication().unregisterBroadcastReceiver();
		registerReceiver(mReceiver, new IntentFilter());
	}

	@Override
	protected void onStop() {
		super.onStop();
		unregisterReceiver(mReceiver);
		getIcuApplication().registerBroadcastReceiver();
	}

	public String getMessage() {
		return mChatInputText.getText().toString();
	}

	public Messageable getFromMessageable() {
		return mFromMessageable;
	}

	public Messageable getToMessageable() {
		return mToMessageable;
	}

	public void resetMessage() {
		mChatInputText.setText(null);
	}

	private class ChatBroadcastReceiver extends BroadcastReceiver {
		private static final String TAG = "ChatActivity.ChatBroadcastReceiver";

		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(PushManager.ACTION_PUSH_RECEIVED)) {
				Log.d(TAG, "Action Push Received");
				// push notification received, perhaps store it in a db
				/* TODO Do something useful with push notification */
				Object message = intent.getExtras().get("message");
				Object from = intent.getExtras().get("from");
				Toast.makeText(context, (String) message, Toast.LENGTH_LONG).show();
				Log.d(TAG, "extra message = " + message + ", extra from = " + from);
			}
		}
	}
}
