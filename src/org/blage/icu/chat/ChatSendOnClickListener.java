package org.blage.icu.chat;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;

public class ChatSendOnClickListener implements OnClickListener {
	private Context mContext;

	public ChatSendOnClickListener(Context context) {
		mContext = context;
	}

	@Override
	public void onClick(View v) {
		ChatActivity chatActivity = (ChatActivity) mContext;
		chatActivity.getIcuApplication().getCommunicationProxy()
				.sendMessage(chatActivity.getFromMessageable(), chatActivity.getFromMessageable(), chatActivity.getMessage());
		chatActivity.resetMessage();
	}
}
