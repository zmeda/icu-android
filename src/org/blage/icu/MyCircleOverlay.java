package org.blage.icu;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.Projection;

public class MyCircleOverlay extends Overlay {
	private ClientMapActivity clientMap;

	private Paint paintStroke;
	private Paint paintFill;
	private GradientDrawable gradientDrawable;
	private Rect rect;

	private GeoPoint myLocationGeoPoint;

	public MyCircleOverlay(Context context) {
		super();

		paintStroke = new Paint();
		paintStroke.setAntiAlias(true);
		paintStroke.setStrokeWidth(2.0f);
		paintStroke.setStyle(Style.STROKE);
		paintStroke.setColor(0xff2222ff);

		paintFill = new Paint(paintStroke);
		paintFill.setStyle(Style.FILL);
		paintFill.setAlpha(0x22);

		clientMap = ((ClientMapActivity) context);

		gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.BL_TR, new int[] { 0x000071BC, 0x880071BC });
		gradientDrawable.setShape(GradientDrawable.OVAL);
		gradientDrawable.setGradientType(GradientDrawable.RADIAL_GRADIENT);
	}

	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow) {
		Projection projection = mapView.getProjection();

		myLocationGeoPoint = clientMap.getMyData().getLocationGeoPoint();

		if (!shadow && myLocationGeoPoint != null && clientMap.getMyFriends() != null && clientMap.getMyFriends().getRadius() > 0) {
			Point myPoint = new Point();
			projection.toPixels(myLocationGeoPoint, myPoint);

			int radiusPixel = (int) (projection.metersToEquatorPixels(clientMap.getMyFriends().getRadius()) * (1 / Math.cos(Math.toRadians(clientMap
					.getMyData().getLocation().getLatitude()))));

			if (radiusPixel > 0) {
				gradientDrawable.setGradientRadius(radiusPixel);
				rect = new Rect(myPoint.x - radiusPixel, myPoint.y - radiusPixel, myPoint.x + radiusPixel, myPoint.y + radiusPixel);
				gradientDrawable.setBounds(rect);
				gradientDrawable.draw(canvas);
			}
		}
		super.draw(canvas, mapView, shadow);
	}
}
